package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();

    	boolean isShowMessageForm = false;
    	User user = (User) request.getSession().getAttribute("loginUser");
    	if(user != null) {
    		isShowMessageForm = true;
    	}

    	String userId = request.getParameter("user_id");
    	String startDate = request.getParameter("start");
    	String endDate = request.getParameter("end");

    	if(!isValidDate(startDate, endDate, errorMessages)) {
    		session.setAttribute("errorMessages", errorMessages);
    		response.sendRedirect("./");
    		return;
    	}

    	List<UserMessage> messages = new MessageService().select(userId, startDate, endDate);
    	List<UserComment> comments = new CommentService().select();

    	request.setAttribute("end", endDate);
    	request.setAttribute("start", startDate);
    	request.setAttribute("messages", messages);
    	request.setAttribute("comments", comments);
    	request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request,  response);
	}

    private boolean isValidDate(String startDate, String endDate, List<String> errorMessages) {

    	if((!StringUtils.isBlank(startDate) && !startDate.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) ||
    			(!StringUtils.isBlank(endDate) && !endDate.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$"))) {
    			errorMessages.add("不正なパラメータが入力されました");
    	}

    	if(errorMessages.size() != 0) {
			return false;
		}
		return true;
    }

}
